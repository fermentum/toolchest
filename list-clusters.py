import boto3

REGIONS=('us-east-1',
        'us-east-2',
        'us-west-1',
        'us-west-2')

def main():
    for region in REGIONS:
        client = boto3.client('emr', region_name = region)
        clusters = client.list_clusters(ClusterStates=['STARTING','BOOTSTRAPPING','RUNNING','WAITING'])
        for cluster in clusters['Clusters']:
            print('%s ==> %s' % (cluster['Name'], cluster['Id']))

if __name__ == '__main__':
    main()
